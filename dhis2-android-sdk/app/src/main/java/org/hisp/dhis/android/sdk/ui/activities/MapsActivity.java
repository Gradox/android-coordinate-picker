package org.hisp.dhis.android.sdk.ui.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.raizlabs.android.dbflow.sql.builder.Condition;
import com.raizlabs.android.dbflow.sql.language.Select;

import org.hisp.dhis.android.sdk.R;
import org.hisp.dhis.android.sdk.persistence.Dhis2Application;
import org.hisp.dhis.android.sdk.persistence.models.Event;
import org.hisp.dhis.android.sdk.persistence.models.Event$Table;
import org.hisp.dhis.android.sdk.ui.adapters.rows.dataentry.CoordinatesRow;
import org.hisp.dhis.android.sdk.ui.views.FloatingActionButton;

import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    GoogleMap map;
    MarkerOptions currentMarker;
    LocationManager locationManager;
    Location location;
    FloatingActionButton finishButton;
    Event event;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        if(getIntent() != null) {
            long id = getIntent().getLongExtra("EVENT_ID", -1);
            System.out.println("ID: " + id);
            event = new Select()
                    .from(Event.class)
                    .where(Condition.column(Event$Table.LOCALID).is(id))
                    .querySingle();
        }
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        finishButton = (FloatingActionButton) this.findViewById(R.id.map_accept_pos);
        mapFragment.getMapAsync(this);
        currentMarker = null;
        Toast.makeText(this, "Tap on the map to choose a position", Toast.LENGTH_LONG).show();
    }

    /**
     * Called when the finished button is clicked
     * @param view
     */
    public void finishedPositioning(View view) {
        if (currentMarker != null) {
            LatLng chosenPosition = currentMarker.getPosition();

            //Give control back to earlier activity and pass data.
            event.setLatitude(chosenPosition.latitude);
            event.setLongitude(chosenPosition.longitude);
            event.save();
            //Dhis2Application.getEventBus().post(new OnCoordsChangedEvent());
            finish();
        }
    }

    public static class OnCoordsChangedEvent {}

    @Override
    public void onMapReady(GoogleMap map) {
        this.map = map;
        //check permissions
        PackageManager pm = getPackageManager();
        if (!(pm.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, getPackageName()) == PackageManager.PERMISSION_GRANTED)) {
            //Create dialog box
            AlertDialog alertDialog = new AlertDialog.Builder(MapsActivity.this).create();
            alertDialog.setTitle("No Permission!");
            alertDialog.setMessage("Could not access the GPS!");
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            alertDialog.show();
        }
        map.setMyLocationEnabled(true);

        //disable buttons for navigation
        map.getUiSettings().setMapToolbarEnabled(false);

        //set current location and zoom in
        location = getLastKnownLocation();
        Double longitude, latitude;
        if (location!=null) {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
            if (longitude != null && latitude != null) {
                if ((latitude >= -90 && latitude <= 90) && (longitude >= -180 && longitude <= 180)) {
                    LatLng myLatLng = new LatLng(latitude, longitude);
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(myLatLng, 10);
                    map.animateCamera(cameraUpdate);
                }
            }
        }

        //Marker
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                //Add marker
                MarkerOptions markerOptions = new MarkerOptions();
                // Which color is best for colorblind people?
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
                markerOptions.position(latLng);
                clearMap();
                animateCam(latLng);
                addMarker(markerOptions);
                finishButton.setVisibility(View.VISIBLE);
                currentMarker = markerOptions;
            }
        });
    }

    private void clearMap() {
        map.clear();
    }

    private void animateCam(LatLng latLng) {
        map.animateCamera(CameraUpdateFactory.newLatLng(latLng));
    }

    private void addMarker(MarkerOptions markerOptions) {
        map.addMarker(markerOptions);
    }

    /**
     * Used by the initial zoom on startup.
     * @return the best last know location
     */
    private Location getLastKnownLocation() {
        locationManager = (LocationManager)getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location location = locationManager.getLastKnownLocation(provider);
            if (location == null) {
                continue;
            }
            if (bestLocation == null || location.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = location;
            }
        }
        return bestLocation;
    }
}