# Android Coordinate Picker #

This repository is for our group-project for the course INF5750 at the University of Oslo, autumn 2015.

The task is to add the additional functionality to the existing DHIS2 android application: allow users to pick coordinates via Google Maps.

### How do I get set up?

Summary of set up

1. Clone the repo

1. Checkout the master branch

1. Run the app and login:

    * server: https://apps.dhis2.org/demo

    * username: android

    * password: Android123